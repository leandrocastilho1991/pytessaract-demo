import cv2
import math
import logging
import numpy as np
from scipy import ndimage
from deskew import determine_skew

logger = logging.getLogger(__name__)

# Método para abrir a imagem
def open_image(image_path: str):
    logger.info('Abrindo Imagem: {}'.format(image_path))
    return cv2.imread(image_path)

# Método para ampliar a imagem
def resize_image(image, fx: float, fy: float):
    logger.info('Ajustando tamanho da image')
    return cv2.resize(image, None, fx=fx, fy=fy, interpolation=cv2.INTER_CUBIC)

# Método para definir a imagem em escala de cinza
def gray_filter(image):
    logger.info('Aplicando filtro cinza')
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Método para filtros de dilatação e erosão
def dilate_and_erode(image):
    logger.info('Aplicando filtro de dilatação e erosão')
    kernel = np.ones((1, 1), np.uint8)
    image_dilated = cv2.dilate(image, kernel, iterations=1)
    return cv2.erode(image_dilated, kernel, iterations=1)

# Método para rotação de imagem
def rotate_image(image):
    logger.info('Verificando necessidade de rotação de imagem')
    img_edges = cv2.Canny(image, 100, 100, apertureSize=3)
    lines = cv2.HoughLinesP(img_edges, 1, math.pi / 180.0, 100, minLineLength=100, maxLineGap=5)

    angles = []

    for [[x1, y1, x2, y2]] in lines:
        cv2.line(image, (x1, y1), (x2, y2), (255, 0, 0), 3)
        angle = math.degrees(math.atan2(y2 - y1, x2 - x1))
        angles.append(angle)

    if angle == 0.0:
        return image

    median_angle = (np.median(angles))
    logger.info('Ângulo de rotação: {}'.format(median_angle))
    image_rotated = ndimage.rotate(image, median_angle)

    validated_angle = determine_skew(image_rotated)
    logger.info('Validação de Ângulo Rotacionado: {}'.format(validated_angle))
    if validated_angle != 0.0 and (validated_angle > 80.0 or validated_angle < -80.0):
        image_rotated = ndimage.rotate(image, median_angle * (-1))

    return image_rotated

# Método para filtro de threshold
def threshold_image(image):
    logger.info('Aplicando metodologia threshold')

    filtered_image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    filtered_image = cv2.adaptiveThreshold(filtered_image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 255, 80)
    return filtered_image

# Método para recortar partes desnecessárias da imagem
def crop_image(image):
    coords = cv2.findNonZero(image)  # Find all non-zero points (text)
    x, y, w, h = cv2.boundingRect(coords)  # Find minimum spanning bounding box
    return image[y:y + h, x:x + w]

# Método para construir bounding box na imagem
def bbox(image):
    bbox_image = image

    cnts = cv2.findContours(bbox_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]

    for c in cnts:
        area = cv2.contourArea(c)
        if area > 10000:
            x,y,w,h = cv2.boundingRect(c)
            cv2.rectangle(bbox_image, (x, y), (x + w, y + h), (36,255,12), 3)

    return bbox_image

#Método que chama o fluxo de otimização da imagem
def optimize_image(image_path):
    logger.info('Início de otimização de imagem')
    opened_image = open_image(image_path)
    resized_image = resize_image(opened_image, 4, 4)

    gray_image = gray_filter(resized_image)
    dilated_eroded_image = dilate_and_erode(gray_image)
    rotated_image = rotate_image(dilated_eroded_image)
    cropped_image = crop_image(rotated_image)
    thresholded_image = threshold_image(cropped_image)

    return thresholded_image