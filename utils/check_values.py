'''CPF = 11
CPF_MASKED = 14
CNPJ = 14
CNPJ_MASKED = 14
RG = 9
RG = 12'''

class ValueChecker:

    _instance = None

    @staticmethod
    def get_instance():
        if ValueChecker._instance is None:
            return ValueChecker()
        else:
            return ValueChecker._instance

    def __init__(self):
        if ValueChecker._instance is not None:
            raise Exception('Esta classe é um singleton')
        else:
            self._document_length_allowed = [9, 11, 12, 14]

    def check_document(self, document):
        array_numbers = [int(s) for s in document.split() if s.isdigit()]
        final_number = ''
        for number in array_numbers:
            final_number += final_number.format('{}', number)

        return len(final_number) in self._document_length_allowed

    def check_product(self, product):
        return product.isalpha() and len(product) > 2

    def check_amount(self, current, new_value):
        return new_value > current