def to_float(value):
    try:
        value_str = value.replace(',', '.').replace(' ', '').replace('R$', '')
        if '.' in value_str:
            return float(value_str)
        else:
            return 0.0
    except Exception as e:
        return 0.0