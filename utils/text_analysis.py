import spacy
import logging

from . import converter
from .check_values import ValueChecker
from spellchecker import SpellChecker

logger = logging.getLogger(__name__)

"""
Esta classe foi construída como singleton para evitar que o treinamento carregado 
pela biblioteca spacy ocorra diversas vezes ao longo do processamento.

A ideia desta classe é carregar o objeto de natural language processing(nlp) com
o treinamento realizado previamente, carregar os validadores de dados(value_checker),
instanciar uma classe que executará um algoritmo que tenta deduzir a palavra lida para 
o cenário onde a grafia não está correta ou não foi lida corretamente.
"""
class TextAnalysis:

    _instance = None

    @staticmethod
    def get_instance():
        if TextAnalysis._instance is None:
            return TextAnalysis()
        return TextAnalysis._instance


    def __init__(self):
        if TextAnalysis._instance is None:
            raise Exception('Esta classe é um singleton')
        else:
            self._nlp = spacy.load('pt_trained/model-best')
            self._value_checker = ValueChecker.get_instance()
            self._spell = SpellChecker(language='pt', distance=1)
            self._doc = None

    #Este método receberá o texto a ser interpretado e carregará os dados na variável doc
    def process(self, text):
        self._doc = self._nlp(text)

    #Este método retornará a loja identificada na nota
    def get_store(self):

        if self._doc is None:
            raise Exception("É necessário processar algum dado antes")

        is_founded = False
        for entity in self._doc.ents:
            if entity.label_ == 'ORG' and is_founded is False:
                logger.info('Loja: {}'.format(entity.text))
                is_founded = True
                break

        if is_founded is False:
            logger.error('Empresa da nota não encontrada')

    #Este método retornará o documento do cliente identificado na nota
    def get_document(self):

        if self._doc is None:
            raise Exception("É necessário processar algum dado antes")

        is_founded = False
        for entity in self._doc.ents:
            if entity.label_ == 'DOC' and self._value_checker.check_document(entity.text):
                logger.info('Documento do Cliente: {}'.format(entity.text))
                is_founded = True
                break

        if is_founded is False:
            logger.warning('Documento do cliente não encontrado')

    #Este método retornará os produtos identificados na nota
    def get_products(self):

        if self._doc is None:
            raise Exception("É necessário processar algum dado antes")

        is_founded = False
        products = []
        for entity in self._doc.ents:
            if entity.label_ == 'PRD' and self._value_checker.check_product(entity.text):
                products.append(entity.text)
                is_founded = True

        product_list = self._spell.known(products)

        presentation='';
        for product in product_list:
           presentation += product + '\n'

        if is_founded is False:
            logger.warning('Produtos não encontrado')
        else:
            logger.info('Produtos da Nota:\n {}'.format(presentation))

    #Este método retornará o valor total da nota
    def get_total_amount(self):

        if self._doc is None:
            raise Exception("É necessário processar algum dado antes")

        current_amount = 0
        for entity in self._doc.ents:

            if entity.label_ == 'VAL':
                amount = converter.to_float(entity.text)
                if self._value_checker.check_amount(current_amount, amount):
                    current_amount = amount

        logger.info('Valor Total da Nota:\n {0:.2f}'.format(current_amount))
