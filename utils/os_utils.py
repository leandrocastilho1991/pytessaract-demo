import os
import logging

logger = logging.getLogger("utils.os_utils")

def mogrify_image(image_path: str, dpi: int):
    command = 'mogrify -set units PixelsPerInch -density %d %s'.format(dpi, image_path)
    logger.debug('Mogrifying command {}'.format(command))
    os.system(command)