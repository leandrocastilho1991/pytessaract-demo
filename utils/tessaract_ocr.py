import pytesseract
import os
import logging
import platform
from utils import image_handler, os_utils

logger = logging.getLogger(__name__)

if platform.system() != "Linux":
    pytesseract.pytesseract.tesseract_cmd = os.getenv('OCR_EXEC_PATH', r'C:\Program Files\Tesseract-OCR\tesseract.exe') 
    TESSDATA_PREFIX = os.getenv('TESSDATA_PREFIX_PATH', r'C:\Users\Arthur\PycharmProjects\pytessaract-demo\testdata\tessdata-main\script') 
    
"""
  Este método inicia o processamento de imagem para obtenção do texto utilizando a biblioteca do tesseract  
"""
def run_analysis(image_path):
    logger.info('Executando OCR')

    custom_config = r'--oem 3 --psm 6 -l por+eng'
    
    filtered_image = image_handler.optimize_image(image_path)

    return pytesseract.image_to_string(filtered_image, config=custom_config)