import json
import argparse

from logging.config import dictConfig
from utils import tessaract_ocr
from utils import TextAnalysis

with open("logging_config.json", 'r') as logging_configuration_file:
    config_dict = json.load(logging_configuration_file)

dictConfig(config_dict)

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--path", required=True,
   help="caminho da imagem que deseja processar")

args = vars(ap.parse_args())

data = tessaract_ocr.run_analysis(args["path"])

# Obtem instancia do analisador de texto
text_analysis = TextAnalysis.get_instance()

text_analysis.process(data)

text_analysis.get_store()
text_analysis.get_document()
text_analysis.get_products()
text_analysis.get_total_amount()

