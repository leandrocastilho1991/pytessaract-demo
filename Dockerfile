FROM python:3.6-buster

WORKDIR /app
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y
RUN pip install --upgrade pip setuptools wheel
RUN apt-get install tesseract-ocr-por -y
RUN apt install tesseract-ocr -y
RUN apt install libtesseract-dev -y
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
RUN python -m spacy download pt_core_news_lg
RUN python trainer.py
RUN python -m spacy train config.cfg --paths.train ./train.spacy --paths.dev ./train.spacy --output pt_trained
CMD [ "python", "./main.py" ]