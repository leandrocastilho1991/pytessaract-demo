# Pytessaract-Demo

Solution Sprint FIAP Módulo 3

### Tecnologias

```
python ^3.6.0
pytesseract
opencv
spacy
```

## Instalação & Inicialização

#### Passo 1

Instale o [Google Tessaract](https://github.com/tesseract-ocr/tesseract). Após a instalação você deverá conseguir chamar o comando ```tessaract``` pela linha de comando, caso isso não aconteça certifique-se de que os binários da engine se encontram em suas variáveis de ambiente.

#### Passo 2

Por padrão o tesseract vem apenas com o idioma inglês instalado, para instalar os demais idiomas no Ubuntu você pode executar o seguinte comando ```sudo apt-get install tesseract-ocr-all``` ou caso deseje instalar uma linguagem específica digite o mesmo comando substituindo ```all``` pela linguagem desejada.

**_Exemplo_**
```sudo apt-get install tesseract-ocr-ita```

**_Observação_**
A quantidade de idiomas desejada pode influenciar diretamente no tempo de instalação do seu pacote de idiomas

O comando ```tesseract --list-langs``` listará os idiomas instalados

#### Passo 3

Instale as libs do python utilizando o seguinte comando na raíz do projeto ```pip install --no-cache-dir -r requirements.txt```


### Passo 4

Instale a biblioteca do [Spacy](https://spacy.io/usage#quickstart)

Em seguida rode o comando do treinador ```python trainer.py```

Ao final do processo um arquivo ```train.spacy``` deverá ter sido gerado na raíz do projeto

Para iniciar a importação do treinamento basta rodar o seguinte comando

```python -m spacy train config.cfg --paths.train ./train.spacy --paths.dev ./train.spacy --output pt_trained```

Após a execução as pastas ```model-best``` e ```model-last``` devem ter sido geradas

## Variáveis de ambiente para cenários diferentes de Linux

| Nome                  | Descrição                                     |
| ----------------------| --------------------------------------------- |
| OCR_EXEC_PATH         | Caminho do executável do tesseract            |
| TESSDATA_PREFIX_PATH  | Caminho dos dados de treinamento do tesseract |

## Instalação com Docker
Build do Container
> ```docker build -t python-ocr .```

Executando Container
>  ```docker run python-ocr```

## Executando o projeto por linha de comando

Obtendo lista de ajuda do sistema
```python main.py -h``` 

 Executando passando path da imagem
```python main.py -p /home/joedoe/image.jpg``` 

## Aplicação Web
Fora desenvolvido uma solução web utilizando bibliotecas Javascript para edição e interpreção via OCR de imagens.
Para acessa-la, basta abrir em um navegador o arquivo localizado em ./public_html/index.html

[Link aplicação WEB](https://image-editor-and-ocr.vercel.app)

## Participaram deste Projeto:
| Nome                           | E-mail                         |
| ------------------------------ | ------------------------------ |
| Arthur Yoit Calhares Shida     | shida93@gmail.com              |
| Julio Cesar Urnau de Almeida   | julio.cesar.08.08.93@gmail.com |
| Leandro de Castilho Pita       | leandro.castilho91@gmail.com   |
| Samuel Leme de Almeida Leite   | samuel.lemeleite@gmail.com     |