import spacy

from spacy.tokens import DocBin

def get_data():
    training_data = [
        ("CNPJ/CPF Consumidor: 064.654.852-88", [(21, 35, "DOC")]),
        ("CNPJ/CPF Consumidor: 94.397.142/0001-88", [(21, 39, "DOC")]),
        ("CPF Consumidor: 064.654.852-88", [(16, 30, "DOC")]),
        ("CPF Consumidor: 06465485288", [(16, 27, "DOC")]),
        ("CNPJ Consumidor: 94.397.142/0001-88", [(17, 35, "DOC")]),
        ("CNPJ Consumidor: 94397142000188", [(17, 31, "DOC")]),
        ("Meu Endereço é São Paulo", [(15, 24, "ADD")]),
        ("Minha Cidade é São Paulo", [(15, 24, "ADD")]),
        ("Meu Telefone é 11987775491", [(15, 26, "ADD")]),
        ("ENDEREÇO: Av. Shishina", [(10, 22, "ADD")]),
        ("Hoje é 10/10/2021", [(7, 17, "DATE")]),
        ("Irei Viajar no dia 05/03/2022", [(19, 29, "DATE")]),
        ("O voo sairá as 09:30:25 do dia 05/07/2025", [(15, 23, "DATE"), (31, 41, "DATE")]),
        ("No dia 06/09/2020 houve uma reunião as 15:30:00", [(7, 17, "DATE"), (39, 47, "DATE")]),
        ("Boleto gerado 06/09/2020 as 15:30:00", [(14, 24, "DATE"), (28, 36, "DATE")]),
        ("O valor total é de R$5,50", [(19, 25, "VAL")]),
        ("O custo deste produto é de R$ 45,73", [(27, 35, "VAL")]),
        ("Coca-Cola a R$8,00 é um absurdo", [(0, 9, "PRD"), (12, 18, "VAL")]),
        ("Minhas compras deram um total de R$288,00", [(33, 41, "VAL")]),
        ("Tenho um total de RS 15,00", [(21, 26, "VAL")]),
        ("As Lojas Americanas estão com promoção", [(9, 19, "ORG")]),
        ("Farei minhas compras de fim de ano na Renner ou na Riachuelo", [(38, 44, "ORG"), (51, 60, "ORG")]),
        ("Cupom Fiscal LARA ACESSORIOS EIRELI ME", [(13, 38, "ORG")]),
        ("Não Haviam mais itens disponíveis nas Casa Bahia", [(38, 48, "ORG")]),
        ("Venderei minhas ações da Renner S.A.", [(25, 36, "ORG")]),
        ("Comprarei ações da Varejo S.A.", [(19, 30, "ORG")]),
        ("Não deixei de conferir o cupom das lojas Renner S.A.", [(41, 52, "ORG")]),
        ("Sua razão social é Lopes E Renato Eireli ME", [(19, 43, "ORG")]),
        ("Será a mais nova companhia Verduras ME", [(27, 38, "ORG")]),
        ("001 530236310 Blusa 1,000Un x 89,90", [(14, 19, "PRD"), (30, 35, "VAL")]),
        ("002 520196220 PERFUME PA 1,000Un x 199,00 F1 A 199,00", [(14, 21, "PRD"), (35, 41, "VAL"), (47, 53, "VAL")]),
        ("007 00606060600 109 ANEL 6.00 - UN X 6.00 02718", [(20, 24, "PRD"), (25, 29, "VAL"), (37, 41, "VAL")]),
        ("001 00606060600 109 ANEL 6.00 - UN X 6.00 02718", [(20, 24, "PRD"), (25, 29, "VAL"), (37, 41, "VAL")]),
        ("001 00606060600 109 ANEL 6,00 - UN X 6,00 02718", [(20, 24, "PRD"), (25, 29, "VAL"), (37, 41, "VAL")]),
        ("001 001234 Coca-Cola JUND 117.00% 5,503", [(11, 20, "PRD"), (34, 39, "VAL")]),
        ("001 001234 Coca-Cola JUND 117.00% 5,503", [(11, 20, "PRD"), (34, 39, "VAL")]),
        ("001 001234 Coca-Cola JUND 117.00% 5,50", [(11, 20, "PRD"), (34, 38, "VAL")]),
        ("001 001234 Coca-Cola JUND 117.00% 5,50", [(11, 20, "PRD"), (34, 38, "VAL")]),
        ("TOTAL R$ 5,50", [(6, 13, "VAL")]),
        ("Dinheiro 288,90", [(9, 15, "VAL")])
    ]
    return training_data

def main():
    db = DocBin()
    nlp = spacy.load('pt_core_news_lg')
    for text, annotations in get_data():
        #print(len(text))
        doc = nlp(text)
        ents = []
        for start, end, label in annotations:
            span = doc.char_span(start, end, label=label)
            ents.append(span)
        doc.ents = ents
        db.add(doc)
    db.to_disk("./train.spacy")

if __name__ == "__main__":
    main()

